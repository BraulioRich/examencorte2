package com.example.examencorte29_3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductosDB {
    private Context context;
    private ProductosDBHelper productosDBHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[] {
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PERECEDERO
    };


    public ProductosDB(Context context) {
        this.context = context;
        productosDBHelper = new ProductosDBHelper(this.context);
    }

    public void openDataBase() {
        db = productosDBHelper.getWritableDatabase();
    }

    public long insertarProducto (Producto p) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto._ID, p.getID());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        return db.insert(DefinirTabla.Producto.TABLE_NAME, null, values);
    }

    public long UpdateProducto(Producto p, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        String criterio = DefinirTabla.Producto._ID + " = " + id;

        return db.update(DefinirTabla.Producto.TABLE_NAME, values, criterio, null);
    }

    public int deleteProducto(long id) {
        String criterio = DefinirTabla.Producto._ID + " = " + id;
        return db.delete(DefinirTabla.Producto.TABLE_NAME, criterio, null);
    }

    public Producto readProducto(Cursor cursor) {
        Producto p = new Producto();
        p.setID(cursor.getInt(0));
        p.setNombre(cursor.getString(1));
        p.setMarca(cursor.getString(2));
        p.setPrecio(cursor.getFloat(3));
        p.setPerecedero(cursor.getInt(4));
        return p;
    }

    public Producto getProducto(long id) {
        SQLiteDatabase db = productosDBHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_NAME,
                columnToRead,
                DefinirTabla.Producto._ID + " = ?",
                new String[] {String.valueOf(id)}, null, null, null );
        if(cursor.moveToFirst()) {
            Producto producto = readProducto(cursor);
            cursor.close();
            return producto;
        } else {
            return null;
        }

    }

    public void cerrarDataBase() {
        productosDBHelper.close();
    }

}

