package com.example.examencorte29_3.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ProductosDBHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE=" TEXT";
    private static final String INTEGER_TYPE=" INTEGER";
    private static final String FLOAT_TYPE=" INTEGER";
    private static final String COMMA=" ,";
    private static final String SQL_CREATE_PRODUCTO=" CREATE TABLE " +
            DefinirTabla.Producto.TABLE_NAME + " (" +
            DefinirTabla.Producto._ID + " INTEGER PRIMARY KEY, "+
            DefinirTabla.Producto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.MARCA + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.PRECIO + FLOAT_TYPE + COMMA +
            DefinirTabla.Producto.PERECEDERO + INTEGER_TYPE + ")";
    private static final String SQL_DELETE_PRODUCTO="DROP TABLE IF EXISTS " +
            DefinirTabla.Producto.TABLE_NAME;
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="ventas.db";
    public ProductosDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(db);
    }
}