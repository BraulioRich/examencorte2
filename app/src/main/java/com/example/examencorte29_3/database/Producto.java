package com.example.examencorte29_3.database;

import java.io.Serializable;

public class Producto implements Serializable {
    private long ID;
    private String nombre;
    private String marca;
    private float precio;
    private int perecedero;

    public Producto() {
    }

    public Producto(long ID, String nombre, String marca, float precio, int perecedero) {
        this.ID = ID;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}

