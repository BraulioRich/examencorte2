package com.example.examencorte29_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examencorte29_3.database.ProductosDB;
import com.example.examencorte29_3.database.Producto;

public class ProductoActivity extends AppCompatActivity {
    EditText codigoEdit, nombreEdit, marcaEdit, precioEdit;
    Button borrarButton, actualizarButton, cerrarButton, btnBuscar;
    RadioGroup rg;
    RadioButton perecederoRadio, nomPerecederoRadio;
    private ProductosDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        db = new ProductosDB(ProductoActivity.this);

        codigoEdit = (EditText) findViewById(R.id.codigoProduct);
        nombreEdit = (EditText) findViewById(R.id.nombreProduct);
        marcaEdit = (EditText) findViewById(R.id.marcaProduct);
        precioEdit = (EditText) findViewById(R.id.precioProduct);

        borrarButton = (Button) findViewById(R.id.btnBorrar);
        actualizarButton = (Button) findViewById(R.id.btnActualizar);
        cerrarButton = (Button) findViewById(R.id.btnCerrar);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);

        rg = (RadioGroup) findViewById(R.id.perecederoGroupProduct);
        perecederoRadio = (RadioButton) findViewById(R.id.perecederoProduct);
        nomPerecederoRadio = (RadioButton) findViewById(R.id.noPerecederoProduct);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if(!codigoEdit.getText().toString().equals("")){
                    Producto producto = db.getProducto(Long.parseLong(codigoEdit.getText().toString()));
                    if(producto != null){
                        nombreEdit.setText(producto.getNombre());
                        marcaEdit.setText(producto.getMarca());
                        precioEdit.setText(String.valueOf(producto.getPrecio()));
                        rg.check((producto.getPerecedero() == 1 ? R.id.perecederoProduct : R.id.noPerecederoProduct));
                    }else{
                        Toast.makeText(ProductoActivity.this, "El codigo es incorrecto o no hay existencia " + Integer.parseInt(codigoEdit.getText().toString()), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ProductoActivity.this, "Introduzca el codigo a buscar", Toast.LENGTH_SHORT).show();
                }
                db.cerrarDataBase();
            }
        });

        borrarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if(!codigoEdit.getText().toString().equals("")) {
                    db.deleteProducto(Long.parseLong(codigoEdit.getText().toString()));
                    db.cerrarDataBase();
                    Toast.makeText(ProductoActivity.this, "Producto borrado con exito", Toast.LENGTH_SHORT).show();
                    limpiar();
                }else{
                    Toast.makeText(ProductoActivity.this, "Introduzca el codigo a buscar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        actualizarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if(!codigoEdit.getText().toString().equals("")) {
                    if(!nombreEdit.getText().toString().equals("")) {
                        Producto nProducto = new Producto();
                        nProducto.setID(Long.parseLong(codigoEdit.getText().toString()));
                        nProducto.setNombre(nombreEdit.getText().toString());
                        nProducto.setMarca(marcaEdit.getText().toString());
                        nProducto.setPrecio(Float.parseFloat(precioEdit.getText().toString()));
                        nProducto.setPerecedero(perecederoRadio.isChecked() ? 1 : 0);
                        db.UpdateProducto(nProducto, nProducto.getID());
                        db.cerrarDataBase();
                        Toast.makeText(ProductoActivity.this, "Producto actualizado con exito", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }else{
                        Toast.makeText(ProductoActivity.this, "El nombre es obligatorio", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ProductoActivity.this, "Introduzca el codigo a actualizar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cerrarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void limpiar() {
        codigoEdit.setText("");
        nombreEdit.setText("");
        marcaEdit.setText("");
        precioEdit.setText("");

        rg.check(R.id.perecederoMain);
    }
}
